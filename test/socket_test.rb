require "socket"
require "test/unit"
module SocketTest

  def setup
    @host = ENV["TEST_HOST"] || '127.0.0.1'
    @srv = TCPServer.new(@host, 0)
    @port = @srv.addr[1]
  end

  def test_start
    sock = MogileFS::Socket.start(@host, @port)
    assert_instance_of MogileFS::Socket, sock, sock.inspect
    begin
      sock.write_nonblock("a")
    rescue Errno::EAGAIN
    end
    thr = Thread.new { @srv.accept }
    accepted = thr.value
    assert_instance_of TCPSocket, accepted, accepted.inspect
    assert_nil sock.close
  end

  def test_new
    sock = MogileFS::Socket.tcp(@host, @port)
    assert_instance_of MogileFS::Socket, sock, sock.inspect
    sock.write_nonblock("a")
    thr = Thread.new { @srv.accept }
    accepted = thr.value
    assert_instance_of TCPSocket, accepted, accepted.inspect
    assert_equal "a", accepted.read(1)
    assert_nil sock.close
  end

  def test_timed_peek
    sock = MogileFS::Socket.tcp(@host, @port)
    accepted = @srv.accept
    buf = ""
    assert_raises(MogileFS::UnreadableSocketError) do
      sock.timed_peek(2, buf, 0.01)
    end
    accepted.write "HI"
    assert_equal "HI", sock.timed_peek(2, buf, 0.1)
    assert_equal "HI", buf
    assert_equal "HI", sock.timed_peek(2, buf)
    assert_equal "HI", sock.timed_read(2, buf)
    accepted.close
    assert_nil sock.timed_peek(2, buf)
  end

  def test_timed_read
    sock = MogileFS::Socket.tcp(@host, @port)
    accepted = @srv.accept
    buf = ""
    assert_raises(MogileFS::UnreadableSocketError) do
      sock.timed_read(2, buf, 0.01)
    end
    accepted.write "HI"
    assert_equal "HI", sock.timed_read(2, buf, 0.1)
    assert_equal "HI", buf
    assert_raises(MogileFS::UnreadableSocketError) do
      sock.timed_read(2, buf, 0.01)
    end
    accepted.close
    assert_nil sock.timed_read(2, buf)
  end

  def test_timed_write
    sock = MogileFS::Socket.tcp(@host, @port)
    accepted = @srv.accept
    buf = "A" * 100000
    written = 0
    assert_raises(MogileFS::RequestTruncatedError) do
      loop do
        assert_equal buf.bytesize, sock.timed_write(buf, 0.01)
        written += buf.size
      end
    end
    tmp = accepted.read(written)
    assert_equal written, tmp.size
  end

  def timed_gets
    sock = MogileFS::Socket.tcp(@host, @port)
    accepted = @srv.accept
    assert_raises(MogileFS::UnreadableSocketError) { sock.timed_gets(0.01) }
    accepted.write "HI"
    assert_raises(MogileFS::UnreadableSocketError) { sock.timed_gets(0.01) }
    accepted.write "\n"
    assert_equal "HI\n", sock.timed_gets
    accepted.close
    assert_nil sock.timed_gets
  end

  def test_read_in_full
    sock = MogileFS::Socket.tcp(@host, @port)
    accepted = @srv.accept
    Thread.new do
      accepted.write "HELLO"
      sleep 0.1
      accepted.write " "
      sleep 0.1
      accepted.write "WORLD!!!"
    end
    buf = sock.read("HELLO WORLD!!!".bytesize)
    assert_equal "HELLO WORLD!!!", buf
  end
end
