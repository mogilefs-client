# -*- encoding: binary -*-
require "./test/setup"

class TestHTTPReader < Test::Unit::TestCase
  def rsock
    host = "127.0.0.1"
    s = TCPServer.new(host, 0)
    th = Thread.new do
      c = s.accept
      c.readpartial(0x666)
      c.write("HTTP/1.0 200 OK\r\nContent-Length: 666\r\n\r\n666")
      c.close
    end
    path = "http://#{host}:#{s.addr[1]}/"
    r = MogileFS::HTTPReader.try(path, 666, nil)
    assert_nil th.value
    assert_kind_of IO, r
    r
  ensure
    s.close
  end

  def test_short_to_s
    r = rsock
    assert_raises(MogileFS::SizeMismatchError) { r.to_s }
    r.close
  end

  def test_short_stream_to
    r = rsock
    assert_raises(MogileFS::SizeMismatchError) { r.stream_to("/dev/null") }
    r.close
  end
end
