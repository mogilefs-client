# -*- encoding: binary -*-
require "./test/fresh"

# cmogstored allows Content-Range on PUT, unlike the original mogstored
class Test_cmogstored < Test::Unit::TestCase
  include TestFreshSetup
  alias setup setup_mogilefs
  alias teardown teardown_mogilefs

  def test_range_put_new_file
    add_host_device_domain
    client = MogileFS::MogileFS.new :hosts => @hosts, :domain => @domain

    io = client.new_file "range0", :largefile => :content_range
    assert_nil io.close
    assert_equal "", client.get_file_data("range0")

    io = client.new_file "writes", :largefile => :content_range
    %w(a b c d e).each { |x| io.write(x) }
    assert_nil io.close
    assert_equal "abcde", client.get_file_data("writes")

    io = client.new_file "puts", :largefile => :content_range
    %w(a b c d e).each { |x| io.puts(x) }
    assert ! client.exist?("puts")
    assert_nil io.close
    assert_equal "a\nb\nc\nd\ne\n", client.get_file_data("puts")
  end

  def test_garbage
    add_host_device_domain
    client = MogileFS::MogileFS.new :hosts => @hosts, :domain => @domain,
                                    :timeout => 60
    nr = 1024 * 1024 * 1024
    client.new_file('giant', :largefile => :stream,
                    :content_length => nr) do |io|
      assert_instance_of MogileFS::NewFile::Stream, io
      zero = Zero.new
      before = GC.count
      wr = IO.copy_stream(zero, io, nr)
      after = GC.count
      assert_equal nr, wr
      assert_in_delta before, after, 1
    end
  end if IO.respond_to?(:copy_stream) && defined?(Zero) &&
         GC.respond_to?(:count) && defined?(RUBY_ENGINE) &&
         RUBY_ENGINE == 'ruby' && ENV['TEST_EXPENSIVE'].to_i != 0

  def test_stream_new_file
    add_host_device_domain
    client = MogileFS::MogileFS.new :hosts => @hosts, :domain => @domain
    client.new_file("chunky", :largefile => :stream) do |io|
      assert_instance_of MogileFS::NewFile::Stream, io
      assert_equal(5, io.write("HELLO"))
      assert_nil io.md5
    end
    assert_equal "HELLO", client.get_file_data("chunky")

    io = client.new_file("puts", :largefile => :stream)
    assert_instance_of MogileFS::NewFile::Stream, io
    assert_equal io, IO.select(nil, [io])[1][0], "IO.select-able"

    assert_nil(io.puts("PUTS!"))
    assert_nil(io.puts("PUTZ"))
    assert_nil io.close
    assert_equal "PUTS!\nPUTZ\n", client.get_file_data("puts")

    io = client.new_file("putc", :largefile => :stream)
    assert_equal(0x20, io.putc(0x20))
    assert_nil io.close
    assert_equal " ", client.get_file_data("putc")

    io = client.new_file("print splat", :largefile => :stream)
    io.print(1, 2, 3)
    assert_nil io.close
    assert_equal "123", client.get_file_data("print splat")

    io = client.new_file("printf", :largefile => :stream)
    assert_nil io.printf("%x", 1638)
    assert_nil io.close
    assert_equal "666", client.get_file_data("printf")

    io = client.new_file("syswrite", :largefile => :stream)
    assert_equal 4, io.syswrite("good")
    assert_equal 7, io.syswrite("morning")
    assert_nil io.close
    assert_equal "goodmorning", client.get_file_data("syswrite")

    io = client.new_file("md5", :largefile=>:stream, :content_md5=>:trailer)
    assert_instance_of Digest::MD5, io.md5
    assert_nil io.puts("HIHI")
    assert_nil io.close
    assert_equal "HIHI\n", client.get_file_data("md5")
    assert_equal Digest::MD5.hexdigest("HIHI\n"), io.md5.hexdigest

    io = client.new_file("<<", :largefile=>:stream)
    assert_equal(io, io << ">>")
    assert_nil io.close
    assert_equal ">>", client.get_file_data("<<")
  end

  def test_stream_new_file_with_content_length
    add_host_device_domain
    client = MogileFS::MogileFS.new :hosts => @hosts, :domain => @domain
    io = client.new_file("clen", :largefile=>:stream,:content_length=>6)
    io << "HIHIHI"
    assert_nil io.close
    assert_equal "HIHIHI", client.get_file_data("clen")

    io = client.new_file("clen", :largefile=>:stream,:content_length=>1)
    io << "FAIL"
    assert_raises(MogileFS::SizeMismatchError) { io.close }
    assert_equal "HIHIHI", client.get_file_data("clen")

    io = client.new_file("md5", :largefile=>:stream,
                                :content_length=>6, :content_md5=>:trailer)
    assert_equal(io, io << "MD5MD5")
    assert_nil io.close
    assert_equal "MD5MD5", client.get_file_data("md5")
    assert_equal Digest::MD5.hexdigest("MD5MD5"), io.md5.hexdigest

    io = client.new_file("md5", :largefile=>:stream,
                                :content_length=>6, :content_md5=>:trailer)
    assert_equal(io, io << "MD5MD")
    assert_raises(MogileFS::SizeMismatchError) { io.close }
    assert_equal Digest::MD5.hexdigest("MD5MD"), io.md5.hexdigest
  end

  def test_md5_check
    add_host_device_domain
    client = MogileFS::MogileFS.new :hosts => @hosts, :domain => @domain
    node = "#@test_host:#@mogstored_http_port"
    pid = fork do
      # not modifying this hash in the same process
      MogileFS::HTTPFile::MD5_TRAILER_NODES[node] = true
      client.store_content("md5_me", nil, "HELLO WORLD")
    end
    _, status = Process.waitpid2(pid)
    assert status.success?, status.inspect
    assert_equal "HELLO WORLD", client.get_file_data("md5_me")
  end

  def setup_mogstored
    @docroot = Dir.mktmpdir(["mogfresh", "docroot"])
    Dir.mkdir("#@docroot/dev1")
    Dir.mkdir("#@docroot/dev2")
    @mogstored_mgmt = TCPServer.new(@test_host, 0)
    @mogstored_http = TCPServer.new(@test_host, 0)
    @mogstored_mgmt_port = @mogstored_mgmt.addr[1]
    @mogstored_http_port = @mogstored_http.addr[1]

    @mogstored_pid = fork do
      mgmt_fd = @mogstored_mgmt.fileno
      http_fd = @mogstored_http.fileno
      args = []
      ENV["CMOGSTORED_FD"] = "#{mgmt_fd},#{http_fd}"
      if @mogstored_mgmt.respond_to?(:close_on_exec=)
        @mogstored_mgmt.close_on_exec = @mogstored_http.close_on_exec = false
        args << { mgmt_fd => mgmt_fd, http_fd => http_fd }
      end
      $stderr.reopen('/dev/null', 'a')
      exec "cmogstored", "--httplisten=#@test_host:#@mogstored_http_port",
           "--mgmtlisten=#@test_host:#@mogstored_mgmt_port",
           "--maxconns=1000", "--docroot=#@docroot", *args
    end
  end
end if `which cmogstored`.chomp.size > 0

# The goal of this is to use a synthetic (non-IO) reader
# to trigger the read/write loop of IO.copy_stream,
# bypassing in-kernel mechanisms like sendfile for zero copy,
# so we wrap the /dev/zero IO object:
class Zero
  def initialize
    @in = File.open('/dev/zero', 'rb')
  end

  def read(len, buf)
    @in.read(len, buf)
  end
end if File.readable?('/dev/zero')
