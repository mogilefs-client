# -*- encoding: binary -*-
require "./test/exec"
require "tmpdir"
require "fileutils"
require "net/http"

module TestFreshSetup
  include TestExec

  def setup_mogilefs(plugins = nil)
    @teardown_pid = $$
    @test_host = "127.0.0.1"
    setup_mogstored
    @tracker = TCPServer.new(@test_host, 0)
    @tracker_port = @tracker.addr[1]

    @dbname = Tempfile.new(["mogfresh", ".sqlite3"], @docroot)
    @mogilefsd_conf = Tempfile.new(["mogilefsd", "conf"], @docroot)
    @mogilefsd_pid = Tempfile.new(["mogilefsd", "pid"], @docroot)
    @dbpath = @dbname.path

    cmd = %w(mogdbsetup --yes --type=SQLite --dbname) << @dbpath
    x!(*cmd)

    @mogilefsd_conf.puts "db_dsn DBI:SQLite:#@dbpath"
    @mogilefsd_conf.write <<EOF
conf_port #@tracker_port
listen #@test_host
pidfile #{@mogilefsd_pid.path}
replicate_jobs 1
fsck_jobs 1
query_jobs 1
mogstored_stream_port #{@mogstored_mgmt_port}
node_timeout 10
EOF
    @mogilefsd_conf.flush

    @trackers = @hosts = [ "#@test_host:#@tracker_port" ]
    @tracker.close
    start_tracker
    @admin = MogileFS::Admin.new(:hosts => @hosts)
    50.times do
      break if File.size(@mogstored_pid.path) > 0
      sleep 0.1
    end unless Integer === @mogstored_pid
  end

  def start_tracker
    x!("mogilefsd", "--daemon", "--config=#{@mogilefsd_conf.path}")
    wait_for_port @tracker_port
  end

  def wait_for_port(port)
    tries = 50
    begin
      TCPSocket.new(@test_host, port).close
      return
    rescue
      sleep 0.1
    end while (tries -= 1) > 0
    raise "#@test_host:#{port} never became ready"
  end

  def add_host_device_domain
    assert_equal [], @admin.get_hosts
    args = { :ip => @test_host, :port => @mogstored_http_port }
    args[:status] = "alive"
    @admin.create_host("me", args)
    assert File.directory?("#@docroot/dev1")
    assert File.directory?("#@docroot/dev2")
    yield_for_monitor_update { @admin.get_hosts.empty? or break }

    me = @admin.get_hosts.find { |x| x["hostname"] == "me" }
    assert_instance_of Hash, me, me.inspect
    assert_kind_of Integer, me["hostid"], me
    assert_equal true, @admin.create_device(me["hostid"], 1)
    yield_for_monitor_update { @admin.get_devices.empty? or break }
    wait_for_usage_file "dev1"
    assert_equal true, @admin.create_device("me", 2)
    wait_for_usage_file "dev2"

    # MogileFS::Server 2.60+ shows reject_bad_md5 monitor status
    dev = @admin.get_devices[0]
    if dev.include?("reject_bad_md5")
      assert [true, false, nil].include?(dev["reject_bad_md5"]), dev.inspect
    end

    out = err = nil
    tries = 0
    begin
      out.close! if out
      err.close! if err
      status, out, err = mogadm("check")
      assert status.success?, status.inspect
      if (tries += 1) > 100
        warn err.read
        puts out.read
        raise "mogadm failed"
      end
      sleep 0.1
    end until out.read =~ /write?able/

    domain = "rbmogtest.#$$"
    @admin.create_domain(domain)
    yield_for_monitor_update { @admin.get_domains.include?(domain) and break }
    @domain = domain
  end

  def teardown_mogilefs
    return if $$ != @teardown_pid
    if Integer === @mogstored_pid
      pid = @mogstored_pid
    else
      pid = File.read(@mogstored_pid.path).to_i
    end
    Process.kill(:TERM, pid) if pid > 0
    if @mogilefsd_pid
      s = TCPSocket.new(@test_host, @tracker_port)
      s.write "!shutdown\r\n"
      s.close
    end
    FileUtils.rmtree(@docroot)
  end

  def wait_for_usage_file(device)
    uri = URI("http://#@test_host:#@mogstored_http_port/#{device}/usage")
    res = nil
    100.times do
      res = Net::HTTP.get_response(uri)
      if Net::HTTPOK === res
        puts res.body if $DEBUG
        return
      end
      puts res.inspect if $DEBUG
      sleep 0.1
    end
    raise "#{uri} failed to appear: #{res.inspect}"
  end

  def setup_mogstored
    @docroot = Dir.mktmpdir(["mogfresh", "docroot"])
    Dir.mkdir("#@docroot/dev1")
    Dir.mkdir("#@docroot/dev2")
    @mogstored_mgmt = TCPServer.new(@test_host, 0)
    @mogstored_http = TCPServer.new(@test_host, 0)
    @mogstored_mgmt_port = @mogstored_mgmt.addr[1]
    @mogstored_http_port = @mogstored_http.addr[1]
    @mogstored_conf = Tempfile.new(["mogstored", "conf"], @docroot)
    @mogstored_pid = Tempfile.new(["mogstored", "pid"], @docroot)
    @mogstored_conf.write <<EOF
pidfile = #{@mogstored_pid.path}
maxconns = 1000
httplisten = #@test_host:#{@mogstored_http_port}
mgmtlisten = #@test_host:#{@mogstored_mgmt_port}
docroot = #@docroot
EOF
    @mogstored_conf.flush
    @mogstored_mgmt.close
    @mogstored_http.close

    x!("mogstored", "--daemon", "--config=#{@mogstored_conf.path}")
    wait_for_port @mogstored_mgmt_port
    wait_for_port @mogstored_http_port
  end
end
