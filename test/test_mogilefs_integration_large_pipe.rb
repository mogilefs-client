# -*- encoding: binary -*-
require './test/fresh'
require "digest/sha1"

class TestMogileFSLargePipe < Test::Unit::TestCase
  include TestFreshSetup
  def setup
    setup_mogilefs
    add_host_device_domain
    @client = MogileFS::MogileFS.new(:hosts => @trackers, :domain => @domain)
  end

  alias teardown teardown_mogilefs

  def test_large_pipe_test
    junk = File.open("/dev/urandom") { |fp| fp.read(1024) }
    junk *= 32
    nr = rand(666) + 1024
    r, w = IO.pipe
    sha1 = Digest::SHA1.new
    th = Thread.new do
      nr.times do
        sha1.update(junk)
        w.write(junk)
      end
      w.close
    end
    assert_equal(nr * junk.size, @client.store_file("a", nil, r))
    r.close
    th.join
    @client.get_file_data("a") do |rd|
      assert_equal(nr * junk.size, @client.store_file("b", nil, rd))
    end
    a = Thread.new { @client.get_file_data("a") { |rd| sha1read(rd) } }
    b = Thread.new { @client.get_file_data("b") { |rd| sha1read(rd) } }
    a = a.value
    b = b.value
    assert_equal a, b
    assert_equal sha1, a

    # We should be able to open FIFOs
    tmp = tmpfile("fifo")
    tmp_path = tmp.path
    File.unlink(tmp_path)
    x!("mkfifo", tmp_path)
    pid = fork do
      File.open(tmp_path, "wb") do |wr|
        nr.times { wr.write(junk) }
      end
    end
    assert_equal(nr * junk.size, @client.store_file("fifo", nil, tmp_path))
    _, status = Process.waitpid2(pid)
    assert status.success?, status.inspect
    fifo_sha1 = @client.get_file_data("fifo") { |rd| sha1read(rd) }
    assert_equal sha1, fifo_sha1
  end

  def sha1read(rd)
    buf = ""
    d = Digest::SHA1.new
    while rd.read(16384, buf)
      d << buf
    end
    d
  end
end
