$LOAD_PATH << 'lib'
require 'mogilefs' # for MogileFS::VERSION
Gem::Specification.new do |s|
  manifest = File.read('.manifest').split(/\n/)
  s.name = 'mogilefs-client'
  s.version = MogileFS::VERSION
  s.executables = %w(mog)
  s.authors = ["#{s.name} hackers"]
  s.summary = 'MogileFS client library for Ruby'
  s.description = <<EOF
A MogileFS client library for Ruby.  MogileFS is an open source
distributed filesystem, see:
https://github.com/mogilefs/MogileFS-Server/wiki for more details.
This library allows any Ruby application to read, write and delete
files in a MogileFS instance.
EOF
  s.email = 'mogilefs-client-public@yhbt.net'
  s.files = manifest
  s.homepage = 'https://yhbt.net/mogilefs-client/'
  s.license = 'BSD-3-Clause'
end
