# -*- encoding: binary -*-
#
# All objects yielded or returned by MogileFS::MogileFS#new_file should
# conform to this interface (based on existing IO methods).  These objects
# should be considered write-only.
module MogileFS::NewFile::Writer

  # see IO#puts
  def puts(*args)
    args.each do |obj|
      write(obj)
      write("\n".freeze)
    end
    nil
  end

  # see IO#putc
  def putc(ch)
    write(ch.respond_to?(:chr) ? ch.chr : ch[0])
    ch
  end

  # see IO#print
  def print(*args)
    args = [ $_ ] unless args[0]
    write(args.shift)
    args.each do |obj|
      write(obj)
      write($,) if $,
    end
    write($\) if $\
    nil
  end

  # see IO#printf
  def printf(*args)
    write(sprintf(*args))
    nil
  end

  # see IO#<<
  def <<(str)
    write(str)
    self
  end

  # This will issue the +create_close+ command to the MogileFS tracker
  # and finalize the creation of a new file.  This returns +nil+ on
  # success and will raise IOError if called twice.  For non-streaming
  # implementations, this will initiate and finalize the upload.
  #
  # see IO#close
  def close
    commit
    nil
  end
end
