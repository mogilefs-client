# -*- encoding: binary -*-
# This is only a hack for old MogileFS installs that didn't have file_info
require "net/http"
require "uri"
module MogileFS::PathsSize
  def self.call(paths)
    errors = {}
    paths.each do |path|
      uri = URI.parse(path)
      begin
        case r = Net::HTTP.start(uri.host, uri.port) { |x| x.head(uri.path) }
        when Net::HTTPOK
          return r['Content-Length'.freeze].to_i
        else
          errors[path] = r
        end
      rescue => err
        errors[path] = err
      end
    end
    errors = errors.map { |path,err| "#{path} - #{err.message} (#{err.class})" }
    raise MogileFS::Error, "all paths failed with HEAD: #{errors.join(', ')}"
  end
end
