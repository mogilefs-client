# -*- encoding: binary -*-
# internal implementation details here, do not rely on this in your code
require "mogilefs/socket_common"
begin
  raise LoadError, "testing pure Ruby version" if ENV["MOGILEFS_CLIENT_PURE"]
  require "mogilefs/socket/kgio"
rescue LoadError
  require "mogilefs/socket/pure_ruby"
end
