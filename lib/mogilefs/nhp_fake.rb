# :enddoc:
require 'net/http'

# This is just for folks that don't have net-http-persistent
class MogileFS::NhpFake # :nodoc:
  attr_accessor :read_timeout, :open_timeout

  def initialize(name)
    @read_timeout = @open_timeout = nil
  end

  def request(uri, req) # :nodoc:
    http = Net::HTTP.new(uri.host, uri.port)
    http.read_timeout = @read_timeout
    http.open_timeout = @open_timeout
    http.request(req)
  end
end
