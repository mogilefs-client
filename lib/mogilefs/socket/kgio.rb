# -*- encoding: binary -*-
# internal implementation details here, do not rely on them in your code
require "kgio"

class MogileFS::Socket < Kgio::Socket
  include MogileFS::SocketCommon

  def self.start(host, port)
    sock = super(Socket.sockaddr_in(port, host))
    sock.post_init(host, port)
  end

  def self.tcp(host, port, timeout = 5)
    sock = start(host, port)
    unless sock.kgio_wait_writable(timeout)
      sock.close
      raise MogileFS::Timeout, 'socket connect timeout'
    end
    sock
  end

  def timed_read(len, dst = "", timeout = 5)
    case rc = kgio_tryread(len, dst)
    when :wait_readable
      kgio_wait_readable(timeout) or unreadable_socket!(timeout)
    else
      return rc
    end while true
  end

  def timed_peek(len, dst, timeout = 5)
    case rc = kgio_trypeek(len, dst)
    when :wait_readable
      kgio_wait_readable(timeout) or unreadable_socket!(timeout)
    else
      return rc
    end while true
  end

  def timed_write(buf, timeout = 5)
    written = 0
    expect = buf.bytesize
    case rc = kgio_trywrite(buf)
    when :wait_writable
      kgio_wait_writable(timeout) or
        request_truncated!(written, expect, timeout)
    when String
      written += expect - rc.bytesize
      buf = rc
    else
      return expect
    end while true
  end
end
